package main

import (
	"fmt"
	"net/http"
	"sort"
	"sync"
	"time"
)

// Constants from the Gitlab API.
const (
	AccessLevelDeveloper = 30
	//AccessLevelMaintainer  = 40
	//AccessLevelOwner       = 50
	VoteUp                           = "thumbsup"
	VoteDown                         = "thumbsdown"
	MergeStatusCanBeMerged           = "can_be_merged"
	PipelineStatusSuccess            = "success"
	PipelineStatusCreated            = "created"
	PipelineStatusWaitingForResource = "waiting_for_resource"
	PipelineStatusPreparing          = "preparing"
	PipelineStatusPending            = "pending"
	PipelineStatusRunning            = "running"
	PipelineStatusManual             = "manual"
	PipelineStatusScheduled          = "scheduled"
)

// User is a Gitlab representation of a user
type User struct {
	ID          int64
	Name        string
	Username    string
	AccessLevel int64 `json:"access_level"` // Only when retrieve members of a project
	// state, avatar_url, web_url seem unrelevant
}

// AwardEmoji is a Gitlab AwardEmoji
type AwardEmoji struct {
	ID            int64
	Name          string
	User          User
	CreatedAt     time.Time `json:"created_at"`
	UpdatedAt     time.Time `json:"updated_at"`
	AwardableID   int64
	AwardableType string
}

// TaskCompletionStatus is a Gitlab task completion status
type TaskCompletionStatus struct {
	Count          int64 `json:"count"`
	CompletedCount int64 `json:"completed_count"`
}

// Discussion is a Gitlab Discussion
type Discussion struct {
	ID             string
	IndividualNote bool   `json:"individual_note"`
	Notes          []Note `json:"notes"`
}

// Note is a Gitlab Discussion Note
type Note struct {
	ID         int64
	Type       string
	Body       string
	Author     User
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
	Resolvable bool
	Resolved   bool
	ResolvedBy *User `json:"resolved_by"`
}

// Pipeline is a Gitlab Pipeline
type Pipeline struct {
	ID        int64
	Sha       string
	Status    string
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`
}

// DiffVersion represent a version of the MR
type DiffVersion struct {
	ID             int64
	HeadCommitSHA  string    `json:"head_commit_sha"`
	BaseCommitSHA  string    `json:"base_commit_sha"`
	StartCommitSHA string    `json:"start_commit_sha"`
	CreatedAt      time.Time `json:"created_at"`
	MergeRequestID int64     `json:"merge_request_id"`
	State          string    `json:"state"`
	RealSize       string    `json:"real_size"`
}

// MergeRequest is a Gitlab MergeRequest
type MergeRequest struct {
	ID                          int64
	IID                         int64 `json:"iid"`
	Title                       string
	Upvotes                     int64
	Downvotes                   int64
	Author                      User
	Labels                      []string             // comma separated list of labels
	WorkInProgress              bool                 `json:"work_in_progress"`
	MergeStatus                 string               `json:"merge_status"` // == "can_be_merged" ?
	CreatedAt                   time.Time            `json:"created_at"`
	UpdatedAt                   time.Time            `json:"updated_at"`
	DiscussionLocked            bool                 `json:"discussion_locked"`
	WebURL                      string               `json:"web_url"`
	TaskCompletionStatus        TaskCompletionStatus `json:"task_completion_status"`
	HasConflicts                bool                 `json:"has_conflicts"`
	BlockingDiscussionsResolved bool                 `json:"blocking_discussions_resolved"`
	HeadPipeline                *Pipeline            `json:"head_pipeline"`
	// Non gitlab fields
	Project     *Project       `json:"-"`
	awardEmojis *[]AwardEmoji  // request cache
	discussions *[]Discussion  // request cache
	notes       *[]Note        // request cache
	versions    *[]DiffVersion // request cache
}

// GetVersions retrieves MR "versions" (for example by force pushing a new version of the branch)
// NOTE: assuming this will be accessed SEQUENTIALLY per MergeRequest
// This will not be "updated" during the whole MR object lifetime
func (mr *MergeRequest) GetVersions() (*[]DiffVersion, error) {
	if mr.versions != nil {
		return mr.versions, nil
	}
	var versions []DiffVersion
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/versions`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &versions); err != nil {
		return nil, err
	}
	// sort versions by Creation time
	sort.Slice(versions, func(i, j int) bool {
		return versions[i].CreatedAt.Before(versions[j].CreatedAt)
	})
	mr.versions = &versions
	return mr.versions, nil
}

// GetPipelines retrieves pipelines related to a MR
func (mr *MergeRequest) GetPipelines() ([]Pipeline, error) {
	var pipelines []Pipeline
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/pipelines`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &pipelines); err != nil {
		return pipelines, err
	}
	return pipelines, nil
}

// GetHeadPipeline retrieves the main pipeline related to a MR
// Head pipeline can be retrieved only with the MR CRUD on the MR single object
func (mr *MergeRequest) GetHeadPipeline() (*Pipeline, error) {
	detailedMR, err := mr.GetDetailedMR()
	if err != nil {
		return nil, err
	}
	return detailedMR.HeadPipeline, nil
}

// GetDetailedMR retrieves detailed info about MR from the single object MR GET rest api
func (mr *MergeRequest) GetDetailedMR() (MergeRequest, error) {
	var detailedMR MergeRequest
	if err := requestJSON(http.MethodGet, mr.GetAPIURL(), map[string]string{}, "", mr.Project.AuthToken, &detailedMR); err != nil {
		return detailedMR, err
	}
	return detailedMR, nil
}

// GetAwardEmojis retrieves emojis "awarded" (added) an MR
// NOTE: assuming this will be accessed SEQUENTIALLY per MergeRequest
// This will not be "updated" during the whole MR object lifetime
func (mr *MergeRequest) GetAwardEmojis() (*[]AwardEmoji, error) {
	if mr.awardEmojis != nil {
		return mr.awardEmojis, nil
	}
	var emojis []AwardEmoji
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/award_emoji`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &emojis); err != nil {
		return nil, err
	}
	// sort emojis by update time
	sort.Slice(emojis, func(i, j int) bool {
		return emojis[i].UpdatedAt.Before(emojis[j].UpdatedAt)
	})
	mr.awardEmojis = &emojis
	return mr.awardEmojis, nil
}

// GetDiscussions retrieves all the discussions included in a MR
// NOTE: assuming this will be accessed SEQUENTIALLY per MergeRequest
// This will not be "updated" during the whole MR object lifetime
func (mr *MergeRequest) GetDiscussions() (*[]Discussion, error) {
	if mr.discussions != nil {
		return mr.discussions, nil
	}
	var discussions []Discussion
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/discussions`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &discussions); err != nil {
		return nil, err
	}
	mr.discussions = &discussions
	return mr.discussions, nil
}

// GetLatestVersionTime gives CreatedAt time of the latest version of the MR.
// This can be considered as the "code updated_at time" of the MR.
func (mr *MergeRequest) GetLatestVersionTime() (time.Time, error) {
	t := time.Time{} // Zero time
	versions, err := mr.GetVersions()
	if err != nil {
		return t, err
	}
	if len(*versions) == 0 {
		return t, fmt.Errorf("Cannot find any version of MergeRequest")
	}
	// versions are already sorted, get the last one
	return (*versions)[len(*versions)-1].CreatedAt, nil
}

// GetNotes retrieves all the notes included in a MR
// It seems notes are spread between "individual" notes and notes from discussions
// NOTE: assuming this will be accessed SEQUENTIALLY per MergeRequest
// This will not be "updated" during the whole MR object lifetime
func (mr *MergeRequest) GetNotes() (*[]Note, error) {
	if mr.notes != nil {
		return mr.notes, nil
	}
	var notes []Note
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/notes`, mr.GetAPIURL()), map[string]string{}, "", mr.Project.AuthToken, &notes); err != nil {
		return nil, err
	}
	mr.notes = &notes
	return mr.notes, nil
}

// GetAllNotes retrieve all notes from discussions and individual notes
func (mr *MergeRequest) GetAllNotes(resolvableOnly bool) (*[]Note, error) {
	notesFromDiscussions, err := mr.GetNotesFromDiscussions()
	if err != nil {
		return notesFromDiscussions, err
	}
	individualNotes, err := mr.GetNotes()
	if err != nil {
		return individualNotes, err
	}
	// remove duplicates
	noteMap := map[int64]Note{}
	for _, note := range append(*notesFromDiscussions, *individualNotes...) {
		if _, found := noteMap[note.ID]; !found {
			if resolvableOnly && !note.Resolvable {
				continue
			}
			noteMap[note.ID] = note
		}
	}
	notes := []Note{}
	for _, value := range noteMap {
		notes = append(notes, value)
	}
	return &notes, nil

}

// GetNotesFromDiscussions get all gitlab "Notes" from discussions
func (mr *MergeRequest) GetNotesFromDiscussions() (*[]Note, error) {
	notes := []Note{}
	discussions, err := mr.GetDiscussions()
	if err != nil {
		return nil, err
	}
	for _, discussion := range *discussions {
		for _, note := range discussion.Notes {
			notes = append(notes, note)
		}
	}
	return &notes, nil
}

// IsAuthorDevOrHigher checks whether the author of the MR has Developer role access level or higher in the project
func (mr *MergeRequest) IsAuthorDevOrHigher() (bool, error) {
	devs, err := mr.Project.GetDevOrHigherByID()
	if err != nil {
		return false, err
	}
	_, ok := devs[mr.Author.ID]
	return ok, nil
}

// CreateNote add a comment in the MR discussion
func (mr *MergeRequest) CreateNote(body string) error {
	var createNote struct {
		Body string `json:"body"`
	}
	createNote.Body = body
	if err := requestJSON(http.MethodPost, fmt.Sprintf(`%s/notes`, mr.GetAPIURL()), map[string]string{}, createNote, mr.Project.AuthToken, nil); err != nil {
		return err
	}
	return nil
}

// HasLabel checks whether the MR has a label associated
func (mr *MergeRequest) HasLabel(label string) bool {
	for _, l := range mr.Labels {
		if l == label {
			return true
		}
	}
	return false
}

// GetLabels returns all MR labels
func (mr *MergeRequest) GetLabels() []string {
	return mr.Labels
}

// AddLabel adds a given label to the MR
func (mr *MergeRequest) AddLabel(label string) error {
	var addLabel struct {
		AddLabels string `json:"add_labels"`
	}
	addLabel.AddLabels = label
	if err := requestJSON(http.MethodPut, mr.GetAPIURL(), map[string]string{}, addLabel, mr.Project.AuthToken, nil); err != nil {
		return err
	}
	return nil
}

// RemoveLabel removes a given label to the MR
func (mr *MergeRequest) RemoveLabel(label string) error {
	var removeLabel struct {
		RemoveLabels string `json:"remove_labels"`
	}
	removeLabel.RemoveLabels = label
	if err := requestJSON(http.MethodPut, mr.GetAPIURL(), map[string]string{}, removeLabel, mr.Project.AuthToken, nil); err != nil {
		return err
	}
	return nil
}

// GetAPIURL provides the MR API root URL
func (mr *MergeRequest) GetAPIURL() string {
	return fmt.Sprintf(`%s/merge_requests/%d`, mr.Project.GetAPIURL(), mr.IID)
}

// Project is an internal representation of a Project
// This is NOT a Gitlab project, only an easy way of storing project connection information
type Project struct {
	ID         int64
	APIRootURL string
	AuthToken  string
	members    *[]User // Members request Cache.
	membersMu  sync.Mutex
}

// GetAPIURL provides the Project API root URL
func (p *Project) GetAPIURL() string {
	return fmt.Sprintf(`%s/projects/%d`, p.APIRootURL, p.ID)
}

// GetMRs retrieves all the MRs inside the project, with given optional filters
func (p *Project) GetMRs(filters map[string]string) ([]MergeRequest, error) {
	var mrs []MergeRequest
	url := fmt.Sprintf(`%s/merge_requests`, p.GetAPIURL())
	if err := requestJSON(http.MethodGet, url, filters, "", p.AuthToken, &mrs); err != nil {
		return mrs, err
	}
	for index := range mrs {
		mrs[index].Project = p
	}
	return mrs, nil
}

// GetAllMembers retrieves project members, including inherited members
// The request will be performed only once if it does not fail
// NOTE: there seem to be no way to filter this list in gitlab API
func (p *Project) GetAllMembers() (*[]User, error) {
	// This "cache" will be used in all MergeRequest goroutines, so we probably need to be safe
	p.membersMu.Lock()
	defer p.membersMu.Unlock()
	if p.members != nil {
		return p.members, nil
	}
	var members []User
	if err := requestJSON(http.MethodGet, fmt.Sprintf(`%s/projects/%d/members/all`, p.APIRootURL, p.ID), map[string]string{}, "", p.AuthToken, &members); err != nil {
		return nil, err
	}
	p.members = &members
	return p.members, nil
}

// GetDevOrHigherByID provides all devs who have an access level higher or equal to a Developer
// Returns a ID -> User map
func (p *Project) GetDevOrHigherByID() (map[int64]User, error) {
	members, err := p.GetAllMembers()
	if err != nil {
		return nil, err
	}
	devs := map[int64]User{}
	for _, member := range *members {
		if member.AccessLevel >= AccessLevelDeveloper {
			devs[member.ID] = member
		}
	}
	return devs, nil
}

// GetConnectedUser retrieve the object representing the connection user based on the authToken
func GetConnectedUser(apiRootURL string, authToken string) (User, error) {
	var user User
	var err error = nil
	err = requestJSON(http.MethodGet, fmt.Sprintf(`%s/user`, apiRootURL), map[string]string{}, "", authToken, &user)
	return user, err
}
