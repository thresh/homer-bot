package main

// Check is an internal representation of a check
type Check struct {
	Description string
	Success     bool
	Reason      string
	Error       error
}

func (check *Check) baseString() string {
	s := check.Description
	if check.Reason != "" {
		s += " - Reason: " + check.Reason
	}
	if check.Error != nil {
		s += " - ERROR: " + check.Error.Error()
	}
	return s
}

// String provides a string representation of the Check. Used in logs
func (check *Check) String() string {
	if check.Success {
		return "OK  - " + check.baseString()
	}
	return "NOK - " + check.baseString()
}

// Markdown provides a markdown representation of the Check. Used in gitlab comment
func (check *Check) Markdown() string {
	if check.Success {
		return ":white_check_mark: " + check.baseString()
	}
	return ":x: " + check.baseString()
}
