# Homer MergeRequest Acceptance process

The Acceptance process slightly differs whether you are a project's *developer* (or higher) or not.

## Voting system

- The **Score** is computed by comparing the number of :thumbsup: and :thumbsdown: given to the MergeRequest
- Only project *developers* (or higher) are taken into account
- No vote (for a Developer MR) means Score = 0


## If the MergeRequest author has Developer role (or higher)

```mermaid
stateDiagram
    Submitted: MR Submitted
    InReview: In Review
    [*] --> Submitted: A VLC developer submitted
    Submitted --> NotCompliant: NOT mergeable by gitlab
    Submitted --> PipelineReady: mergeable by gitlab
    PipelineReady --> NotCompliant: CI NOK
    PipelineReady --> Reviewable: CI OK
    Reviewable --> InReview: Thread opened\nor\nAnother Developer voted
    Reviewable --> Accepted: No vote\nNo thread opened\nduring 72h
    InReview --> Acceptable: All threads resolved\nand\nScore >= 0
    Acceptable --> Accepted: Wait for 24h
    Acceptable --> InReview: A thread has been (re)opened\nor\nScore < 0
    Accepted --> Merged: A Developer launches rebase and merge when pipeline succeeds
    Accepted --> InReview: A thread has been (re)opened\nor\nlen(votes) = 0\nor\nScore < 0
    Merged --> [*]
```

## If the MergeRequest author is **NOT** a project's developer

```mermaid
stateDiagram
    Submitted: MR Submitted
    InReview: In Review
    [*] --> Submitted: A VLC developer submitted
    Submitted --> NotCompliant: NOT mergeable by gitlab
    Submitted --> PipelineReady: mergeable by gitlab
    PipelineReady --> NotCompliant: CI NOK
    PipelineReady --> Reviewable: CI OK
    Reviewable --> InReview: Thread opened\nor\nAnother Developer voted
    InReview --> Acceptable: All threads resolved\nand\nlen(votes) > 0\nand\n Score > 0
    Acceptable --> Accepted: Wait for 72h
    Acceptable --> InReview: A thread has been (re)opened\nor\nlen(votes) = 0\nor\nScore <= 0
    Accepted --> Merged: A Developer launches rebase and merge when pipeline succeeds
    Accepted --> InReview: A thread has been (re)opened\nor\nlen(votes) = 0\nor\nScore <= 0
    Merged --> [*]
```

## Notes

:warning: Do not close an unresolved thread of your own MRs, unless you are the one who started this thread: the MRs will be considered as **Invalid**.

